//
//  AttendanceRecordViewController.h
//  wspace
//
//  Created by Hand02 on 2019/2/25.
//  Copyright © 2019 wspace. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AttendanceRecordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
