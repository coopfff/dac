//
//  ManageList1TableViewCell.h
//  wspace
//
//  Created by Hand02 on 2019/2/26.
//  Copyright © 2019 wspace. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ManageList1TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *operateBtn;

@end

NS_ASSUME_NONNULL_END
