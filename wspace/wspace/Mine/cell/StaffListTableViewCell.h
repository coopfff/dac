//
//  StaffListTableViewCell.h
//  wspace
//
//  Created by Hand02 on 2019/2/27.
//  Copyright © 2019 wspace. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StaffListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *staffLabel;

@end

NS_ASSUME_NONNULL_END
