//
//  ResourcesViewController.h
//  wspace
//
//  Created by 汉德 on 2019/2/14.
//  Copyright © 2019 wspace. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ResourcesViewController : UIViewController
@property (strong,nonatomic) UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
