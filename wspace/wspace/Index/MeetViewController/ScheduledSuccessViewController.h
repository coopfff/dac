//
//  ScheduledSuccessViewController.h
//  wspace
//
//  Created by 汉德 on 2019/2/26.
//  Copyright © 2019 wspace. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ScheduledSuccessViewController : UIViewController

@property (strong, nonatomic) NSMutableDictionary *dataDic;
@end

NS_ASSUME_NONNULL_END
