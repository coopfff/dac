//
//  main.m
//  wspace
//
//  Created by 汉德 on 2019/2/12.
//  Copyright © 2019 wspace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
